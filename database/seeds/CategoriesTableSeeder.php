<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete table if exist
        //DB::statement('SET FOREIGN_KEY_CHECKS=0');

        //DB::table('categories')->truncate();

        $categories = array(
          ['categoryName' => 'Internal News', 'description' => 'News only for internal users'],
          ['categoryName' => 'External News', 'description' => 'News for both internal external users']
        );

        foreach ($categories as $category)
        {
          Category::create($category); //seed categories
        }

    }
}
