<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $password = \Hash::make('Admin@123');

            User::create([
              'surname' => 'Admin',
              'othernames' => 'User',
              'mobile' => '+234-806-878-278',
              'email' => 'admin@gabrielali.com',
              'password' => $password,
            ]);
    }
}
