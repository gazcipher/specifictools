<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $roles = array(
          ['name' => 'User', 'role_description' => 'Users Role, can peform normal users operations'],
          ['name' => 'Writer', 'role_description' => 'Write Role, can peform Adding and Editing, Deleting news operations'],
          ['name' => 'Administrator', 'role_description', 'Global Administrator, can do anything within the system']
        );

        foreach ($roles as $role)
        {
          Role::create($role);
        }
    }
}
