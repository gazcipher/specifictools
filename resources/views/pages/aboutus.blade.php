@extends('partials.uimain')


@section('stylesheets')


@endsection

@section('content')


<div class="container space-top">
              <h1 class="text-center main-color">About Us</h1>
              <div class="col-md-12 col-sm-12 col-lg-12">

                <h3 class="main-color">Who we are?</h3>
                  <p class="text-justify">
                  The company started its operations in 2007. Full activities commenced in May of 2009. The board of directors and the team consists of experienced and accredited professionals. Companies rely on our integrated facility management and engineering services to optimize productivity and to sustain organizations' assets, thus enabling our clients to concentrate on their core line of business. We are liked with global suppliers of choice for innovative technology backed by the highest level of service and support. <strong>Specific Tools and Techniques Limited</strong>, is established to provide total facilities and equipment maintenance services to companies in the telecommunications, energy, private and public sectors of the Nigerian economy. The company is made up of specialists maintenance teams that provide robust and complete nationwide maintenance services. ST&T has gained significant experience from providing maintenance services to leading companies in Nigeria. This places ST&T in a strategic position to meet all clients' facilities need.
                </p>
                <h1 class="main-color text-center"> Our Mission and Vision Statement</h1>
                <p>
                  <strong>Our Vision</strong>, is to eliminate down time in infrastructural management through innovation and application of technology.<br/>
                  <strong>Our Mission</strong>, is to provide engineering solutions and facility management for the benefit of our stakeholders and the environment.
                </p>

              <h1 class="text-center main-color">Our Core Values</h1>

                 <h3 class="main-color">We are Customer Driven</h3>
                    <ul>
                          <li>We Listen</li>
                          <li>We respond to Requests</li>
                          <li>Beyond Satisfactory</li>
                          <li>More value for Money</li>
                      </ul>

                  <h3 class="main-color">Our Innovativeness is second to None</h3>
                    <ul>
                         <li>We are the First to Know</li>
                         <li>We invent new Technology</li>
                         <li>Beats your immagination</li>
                         <li>Surpass competitors</li>
                     </ul>

                  <h3 class="main-color">Our Respect for Individuals is Matchless</h3>
                    <ul>
                         <li>Courtesy in Business</li>
                         <li>Maintainance of Standards</li>
                         <li>Maintainance Integrity</li>
                         <li>Prompt Action</li>
                     </ul>

                <h3 class="main-color">We uphold Safety of Stakeholders Absolutely</h3>
                    <ul>
                          <li>We keep to Specifications</li>
                          <li>Protects your Interest</li>
                          <li>Respond Quick to Emergencies</li>
                          <li>Very Reliable</li>
                      </ul>
                </div>


</div>


@endsection

@section('scripts')

  <script type="text/javascript">
          $(document).ready(function() {
        $('.collapse.in').prev('.panel-heading').addClass('active');
        $('#accordion, #bs-collapse')
        .on('show.bs.collapse', function(a) {
        $(a.target).prev('.panel-heading').addClass('active');
        })
        .on('hide.bs.collapse', function(a) {
        $(a.target).prev('.panel-heading').removeClass('active');
        });
        });
  </script>


@endsection
