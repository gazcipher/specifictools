@extends('partials.uimain')



@section('content')


<div class="container space-top">
  <h2 class="text-center text-danger">Reach Out to Us</h2>
</div>

    <div class="containter" id="google-map">

      <!-- this should have HQ Google static map snapshot -->



    </div>


    <div class="row">
      <div class="col-4 offset-2">
        <div class="card">
          <div class="card-body">
            <h3 class="card-title"><i class="fa fa-map-marker"></i>&nbsp;Lagos State</h5>
            <p class="card-text">
              <address>
                51B Adekunle Bisi Street, <br/>Hill View Estate Ogudu GRA, Ojota, Lagos
              </address>

            </p>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="card">
          <div class="card-body">
            <h3 class="card-title"><i class="fa fa-map-marker"></i>&nbsp;Imo State</h5>
            <p class="card-text">
              <address>
                Oshara Road, Off Poly FUTO Road, <br/>by Ihiagwa Bustop, Owerri
              </address>

            </p>
          </div>
        </div>
      </div>

      <div class="col-4 offset-2">
        <div class="card">
          <div class="card-body">
            <h3 class="card-title"><i class="fa fa-map-marker"></i>&nbsp;Bayelsa State</h5>
            <p class="card-text">
              <address>
                EBI Mechanic Road, <br/>Amarata, Yenagoa, Bayelsa.
              </address>

            </p>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="card">
          <div class="card-body">
            <h3 class="card-title"><i class="fa fa-map-marker"></i>&nbsp;Cross River State</h5>
            <p class="card-text">
              <address>
                11 Ananasa Road, <br/>MCC, Calabar.
              </address>

            </p>
          </div>
        </div>
      </div>


      <div class="col-4 offset-2">
        <div class="card">
          <div class="card-body">
            <h3 class="card-title"><i class="fa fa-map-marker"></i>&nbsp;Akwa Ibom State</h5>
            <p class="card-text">
              <address>
                8, Afhuakwa, Eket, <br/>Akwa Ibom State.
              </address>

            </p>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="card">
          <div class="card-body">
            <h3 class="card-title"><i class="fa fa-map-marker"></i>&nbsp;Aba State</h5>
            <p class="card-text">
              <address>
                21, Ukaegbu Street, <br/>Ogbor Hill, Aba.
              </address>

            </p>
          </div>
        </div>
      </div>


      <div class="col-8 offset-2">
        <div class="card">
          <div class="card-body">
            <h3 class="card-title"><i class="fa fa-map-marker"></i>&nbsp;Rivers State</h5>
            <p class="card-text">
              <address>
                Portharcourt: 18, Shedrack Avenue, <br/>Off Rock Word Bustop, Akpajio/Elelewon Road, Old Refinery Road, Via Oil Mill Bustop, Port Harcourt.
              </address>
              <address>
                <strong>Ahoada:</strong> Mathew's Compound, Off Iloma Street Hospital Road, Ahoada.
              </address>
              <address>
                <strong>Omoku:</strong> Obohia Road, Old Genesis Hotal, omoku, Elele Sokoya Estate, Obelle Ibaa Road, Elele.
              </address>
              <address>
                <strong>Degema:</strong> No. 25 Emmanuel Street Abonnema, Kalabar
              </address>

            </p>
          </div>
        </div>
      </div>


    </div>

    </div>

    </div>



@endsection

@section('javascripts')



@endsection
