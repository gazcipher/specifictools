@extends('partials.uimain')


@section('stylesheets')
  <!-- This is for the second accordion -->

  <style>

          body {
          margin: 0;
          font-family: 'Roboto';
          font-size: 14px;
          /*background: #455A64;*/
          background: #fff;
        }

        h3 {
          color: #fff;
          font-size: 24px;
          text-align: center;
          margin-top: 30px;
          padding-bottom: 30px;
          border-bottom: 1px solid #eee;
          margin-bottom: 30px;
          font-weight: 300;
        }

        .container {
          max-width: 970px;
        }

        div[class*='col-'] {
          padding: 0 30px;
        }

        .wrap {
          box-shadow: 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 1px 5px 0px rgba(0, 0, 0, 0.12);
          border-radius: 4px;
        }

        a:focus,
        a:hover,
        a:active {
          outline: 0;
          text-decoration: none;
        }

        .panel {
          border-width: 0 0 1px 0;
          border-style: solid;
          border-color: #fff;
          background: none;
          box-shadow: none;
        }

        .panel:last-child {
          border-bottom: none;
        }

        .panel-group > .panel:first-child .panel-heading {
          border-radius: 4px 4px 0 0;
        }

        .panel-group .panel {
          border-radius: 0;
        }

        .panel-group .panel + .panel {
          margin-top: 0;
        }

        .panel-heading {
          background-color: #009688;
          border-radius: 0;
          border: none;
          color: #fff;
          padding: 0;
        }

        .panel-title a {
          display: block;
          color: #fff;
          padding: 15px;
          position: relative;
          font-size: 16px;
          font-weight: 400;
        }

        .panel-body {
          background: #fff;
        }

        .panel:last-child .panel-body {
          border-radius: 0 0 4px 4px;
        }

        .panel:last-child .panel-heading {
          border-radius: 0 0 4px 4px;
          transition: border-radius 0.3s linear 0.2s;
        }

        .panel:last-child .panel-heading.active {
          border-radius: 0;
          transition: border-radius linear 0s;
        }
        /* #bs-collapse icon scale option */

        .panel-heading a:before {
          content: '\e146';
          position: absolute;
          font-family: 'Material Icons';
          right: 5px;
          top: 10px;
          font-size: 24px;
          transition: all 0.5s;
          transform: scale(1);
        }

        .panel-heading.active a:before {
          content: ' ';
          transition: all 0.5s;
          transform: scale(0);
        }

        #bs-collapse .panel-heading a:after {
          content: ' ';
          font-size: 24px;
          position: absolute;
          font-family: 'Material Icons';
          right: 5px;
          top: 10px;
          transform: scale(0);
          transition: all 0.5s;
        }

        #bs-collapse .panel-heading.active a:after {
          content: '\e909';
          transform: scale(1);
          transition: all 0.5s;
        }
        /* #accordion rotate icon option */

        #accordion .panel-heading a:before {
          content: '\e316';
          font-size: 24px;
          position: absolute;
          font-family: 'Material Icons';
          right: 5px;
          top: 10px;
          transform: rotate(180deg);
          transition: all 0.5s;
        }

        #accordion .panel-heading.active a:before {
          transform: rotate(0deg);
          transition: all 0.5s;
        }

        .panel-body{
          padding: 15px !important;
        }
        .shift-up{
          margin-top: -20px;
        }
        .shift-up2{
          margin-top: -35px;
        }
        .panel-heading {
          background-color: #990000 !important;
          border-radius: 0;
          border: none;
          color: #fff;
          padding: 0;
        }

  </style>


@endsection

@section('content')


<div class="container space-top">


            <div class="container space-top">
              <h2 class="text-center text-danger">Leadership Team</h2>
                  <div class="col-md-12 col-sm-12 col-lg-12">
                  <!--<h3>Default collapse with scaling icon</h3>-->
                  <div class="panel-group wrap" id="bs-collapse">

                  <div class="panel">
                  <div class="panel-heading">
                  <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#" href="#StaffOne">
                  Obafemi Owopetu -  <span>M.D. / Chief Executive Officer</span>
                  </a>
                  </h4>
                  </div>
                  <div id="StaffOne" class="panel-collapse collapse">
                  <div class="panel-body text-justify">
                    He holds a degree in Economics from the Ondo State University (1988)
                              and an OMP graduate from Lagos Business School. A fellow of the Institute
                              of Chattered Accountants of Nigeria. He worked with reputable consultancy firm- Coopers Lybrand as a Senior Consultant
                              for several years. His Telecom experience started as the Northern Cell Site maintenance contractor for Globacom Limited. He was Chief Operating Officer of Equideb Associates & Co- a technology and talent driven
                              Consultancy outfit. He has attended several local and international courses and seminars on project, maintenance management and Information Technology.
                  </div>
                  </div>

                  </div>
                  <!-- end of panel -->

                  <div class="panel">
                  <div class="panel-heading">
                  <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#" href="#StaffTwo">
                  Daramola Sheriffdeen Alao - <span>E.D. / Chief Operating Officer</span>
                  </a>
                  </h4>
                  </div>
                  <div id="StaffTwo" class="panel-collapse collapse">
                  <div class="panel-body text-justify">
                    Daramola, S. A. is a Facility Management Professional, an Engineer. He obtained
                              a B.Engr degree in Mechanical/Production Engineering
                              from the Abubakar Tafawa Balewa University Bauchi and P.gD in Management from
                              the Lagos State University, Ojo. He is an OMP graduate from Lagos Business School (PAN African University). He has over 19 years work experiences in Electricity power generation and Distribution, Real Estate
                              and Consultancy in Health Sector while in Bayelsa State Electricity Board, Stallion Property & Development Limited, Amalgamated Estates Limited
                              and Family Health International respectively. His specialty includes generation and distribution of electricity power with Gas Turbines, maintenance and management of engineering equipment, integrated facility management of building systems, re-design of Public Health Care facility to comply with work place system.
                              Currently, he manages and maintains electro-mechanical telecom infrastructures. Daramola Sheriff is a member of the Nigerian Society of Engineers; A member of International Facility Management Professioanl, Houston, USA.
                  </div>

                  </div>
                  </div>
                  <!-- end of panel -->

                  <div class="panel">
                  <div class="panel-heading">
                  <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#" href="#StaffThree">
                  Adewale Adejumobi -  <span>Commercial Manager</span>
                  </a>
                  </h4>
                  </div>
                  <div id="StaffThree" class="panel-collapse collapse">
                  <div class="panel-body text-justify">
                    Adewale Adejumobi joined Specific Tools & Techniques Ltd in 2012 as the Project Assistant.
                             He is a graduate of Civil/Structural Engineering, from
                             the Lagos State Polytechnic. He is experienced Civil Engineering Design & Construction and Project Management. He is a trained
                             Structural Design Software and Project manegement. Adewale Adejumobi is an Associate Member of the Chattered Institute of Project Management.
                  </div>
                  </div>
                  </div>
                  <!-- end of panel -->

                  <div class="panel">
                  <div class="panel-heading">
                  <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#" href="#StaffFour">
                  Adewale Shofola - <span>Legal & Coporate Affairs Manager</span>
                  </a>
                  </h4>
                  </div>
                  <div id="StaffFour" class="panel-collapse collapse in">
                  <div class="panel-body text-justify">
                    He obtained a Bachelor of Law (LLB Hons.) degree (Second Class Lower Division) from the University of Lagos
                              in 1998 and was called to the Nigerian Bar a year later in 1999 with a Second Class Upper Division. Fola worked as a junior in chambers before joining the legal Department of Stallion
                              Property & Development Company Limited ("Stallion"), a Subsidiary of the Nigerian National
                              Petroleum Corporation (NNPC), as a Legal Officer. A Certified Project Manager. Fola is a PRINCE 2 practitioner uses the methodology in the planning
                              and implementation of Regulatory Compliance and the delivery of company projects at Specific Tools & Techniques Limited.
                              He is a member of the Nigerian Institute of Management (2004), and Associate Member of the Chartered of Arbitrators (UK) (2010).
                  </div>
                  </div>
                  </div>
                  <!-- end of panel -->
                  <!-- end of panel -->

                  <div class="panel">
                  <div class="panel-heading">
                  <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#" href="#StaffFive">
                Tade Anjonrin-Ohu - <span>Strategy and Business Development</span>
                  </a>
                  </h4>
                  </div>
                  <div id="StaffFive" class="panel-collapse collapse in">
                  <div class="panel-body text-justify">
                  A graduate of Mathematics from University of Ibadan and a second degree in Information Systems in UK. He is an experienced Project Analyst and Business Development Manager. He was the Project & Operations Manager with Nicities Nigeria Limited responsible for the Environmental Impact Assessment for Etisalat Nigeria. He was also the Project Officer at Learning and Skilled Networks (UK). He has attended many local and international trainings and courses on Project Management, Supply-Chain Management, Conflict Management and Occupational Health & Safety.
                  </div>
                  </div>
                  </div>
                  <!-- end of panel -->
                  <div class="panel">
                  <div class="panel-heading">
                  <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#" href="#StaffSix">
                  Abiodun Olofinlade - <span>National Project Manager: Active Maintenence</span>
                  </a>
                  </h4>
                  </div>
                  <div id="StaffSix" class="panel-collapse collapse in">
                  <div class="panel-body text-justify">
                    Abiodun Olofinlade is a graduate with a B. Sc degree in Computer Science from University of Ado-Ekiti.
                             He joined Specific Tools and Techniques Limited in 2014 as a Wireless Engineer after gathering pools of experiences
                             from Alcatel - Lucent Nig. Ltd for 4 years. He is a Certified RF wireless expert, Radio Network Planning, BTS & MW Link Designer.
                  </div>
                  </div>
                  </div>
                  <!-- end of panel -->

                  </div>
                  <!-- end of #bs-collapse  -->

                  </div>



                  </div>
                  <!-- end of container -->

</div>


@endsection

@section('scripts')

  <script type="text/javascript">
          $(document).ready(function() {
        $('.collapse.in').prev('.panel-heading').addClass('active');
        $('#accordion, #bs-collapse')
        .on('show.bs.collapse', function(a) {
        $(a.target).prev('.panel-heading').addClass('active');
        })
        .on('hide.bs.collapse', function(a) {
        $(a.target).prev('.panel-heading').removeClass('active');
        });
        });
  </script>


@endsection
