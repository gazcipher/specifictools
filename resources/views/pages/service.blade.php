@extends('partials.uimain')


@section('content')


<div class="container space-top">
  <h2 class="text-center text-danger">Our Services</h2>





    <!--Accordion wrapper-->
  <div class="accordion md-accordion accordion-3 z-depth-1-half" id="accordionEx194" role="tablist"
  aria-multiselectable="true">

  <!-- <ul class="list-unstyled d-flex justify-content-center pt-5 red-text">
  <li><i class="fas fa-anchor mr-3 fa-2x" aria-hidden="true"></i></li>
  <li><i class="far fa-life-ring mr-3 fa-2x" aria-hidden="true"></i></li>
  <li><i class="far fa-star fa-2x" aria-hidden="true"></i></li>
  </ul> -->

  <h2 class="text-center text-uppercase red-text py-4 px-3">What We Do!</h2>

  <hr class="mb-0">

  <!-- Accordion card -->
  <div class="card">

  <!-- Card header -->
  <div class="card-header" role="tab" id="heading4">
    <a data-toggle="collapse" data-parent="#accordionEx194" href="#collapse4" aria-expanded="true"
      aria-controls="collapse4">
      <h3 class="mb-0 mt-3 red-text">
        Turnkey Supply & Deployment of Telecom Infrastructure <i class="fas fa-angle-down rotate-icon fa-2x"></i>
      </h3>
    </a>
  </div>

  <!-- Card body -->
  <div id="collapse4" class="collapse show" role="tabpanel" aria-labelledby="heading4" data-parent="#accordionEx194">
    <div class="card-body pt-0">
      <h1 class="main-color">Outdoor Infrastructure</h1>
      <ul>
        <li>Telecommunication Towers</li>
        <li>Telecommunications Housing & Shelters</li>
        <li>Microwave Equipement</li>
        <li>Fibre Optic</li>
      </ul>
      <h1 class="main-color">Network Implementation (I.T. & Communication)</h1>
      <ul>
        <li>Network design, planning anmd Optimization</li>
      </ul>
      <h1 class="main-color">Indoor Equipement</h1>
      <ul>
        <li>Procurement and Maintenance</li>
      </ul>
    </div>
  </div>
  </div>
  <!-- Accordion card -->

  <!-- Accordion card -->
  <div class="card">

  <!-- Card header -->
  <div class="card-header" role="tab" id="heading5">
    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse5" aria-expanded="false"
      aria-controls="collapse5">
      <h3 class="mb-0 mt-3 red-text">
        Operations and Maintenance of Telecom Infrastructure <i class="fas fa-angle-down rotate-icon fa-2x"></i>
      </h3>
    </a>
  </div>

  <!-- Card body -->
  <div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="heading5" data-parent="#accordionEx194">
    <div class="card-body pt-0">
      <p>With our enriched experience, technical & functional expertise in Operations and Maintenance, we have gained highly satisfied clients by ensuring smooth service to our clients 24 hours of the day, 365 days a year. We carry out the following scheduled and corrective maintenance services.</p>
      <ul>
        <li>Mechanical, Electrical & Civil Maintenance services</li>
        <li>HVAC Maintenance Services</li>
        <li>DC Power Systems Maintenance Services (Inverters, Batteries, UPS, etc)</li>
        <li>AC Power Systems Maintenance Services (generator, AVR, Utility bills payment etc)</li>
        <li>Janitorial and Batting Maintenance Services</li>
        <li>Fire & alarm Systems Maintenance Services</li>
        <li>Fuel Management</li>
        <li>Site Security & Community Liaison Management Services</li>
      </ul>
    </div>
  </div>
  </div>
  <!-- Accordion card -->

  <!-- Accordion card -->
  <div class="card">

  <!-- Card header -->
  <div class="card-header" role="tab" id="heading6">
    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse6" aria-expanded="false"
      aria-controls="collapse6">
      <h3 class="mb-0 mt-3 red-text">
        Integrated Facilities Management & Maintenance <i class="fas fa-angle-down rotate-icon fa-2x"></i>
      </h3>
    </a>
  </div>

  <!-- Card body -->
  <div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="heading6" data-parent="#accordionEx194">
    <div class="card-body pt-0">
      <p>We are well-equipped to handle installation, preventive and corrective maintenance services of facilities infrastructure and ensure that they all have a 100% uptime and environment compliance. Our maintenance unit ensures that required tests are carried out and sites are verified before and after construction. They are responsible for ensuring the availability of utilities daily. Bearing in mind our client's needs and taking into consideration environmental and situational factors we are able to look at the whole spectrum of the clients needs, making us adequately able to provide all the technical support required.</p>
    </div>
  </div>
  </div>
  <!-- Accordion card -->
  <!-- Accordion card -->
  <div class="card">

  <!-- Card header -->
  <div class="card-header" role="tab" id="heading9">
    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse9" aria-expanded="false"
      aria-controls="collapse9">
      <h3 class="mb-0 mt-3 red-text">
        Take a look at our Civil Works. We are certain you will be impressed. <i class="fas fa-angle-down rotate-icon fa-2x"></i>
      </h3>
    </a>

  </div>

  <!-- Card body -->
  <div id="collapse9" class="collapse" role="tabpanel" aria-labelledby="heading9" data-parent="#accordionEx194">
    <div class="card-body pt-0">
      <p>With our enriched experience, technical & functional expertise in Operations and Maintenance, we have gained highly satisfied clients by ensuring smooth service to our clients 24 hours of the day, 365 days a year. We carry out the following scheduled and corrective maintenance services.</p>
      <ul>
        <li>Mechanical, Electrical & Civil Maintenance services</li>
        <li>HVAC Maintenance Services</li>
        <li>DC Power Systems Maintenance Services (Inverters, Batteries, UPS, etc)</li>
        <li>AC Power Systems Maintenance Services (generator, AVR, Utility bills payment etc)</li>
        <li>Janitorial and Batting Maintenance Services</li>
        <li>Fire & alarm Systems Maintenance Services</li>
        <li>Fuel Management</li>
        <li>Site Security & Community Liaison Management Services</li>
      </ul>
    </div>
  </div>
  </div>
  <!-- Accordion card -->
  <div class="card">

  <!-- Card header -->
  <div class="card-header" role="tab" id="heading10">
    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse10" aria-expanded="false"
      aria-controls="collapse10">
      <h3 class="mb-0 mt-3 red-text">
        Diesel Generator Procurement, Installation and Maintenance. <i class="fas fa-angle-down rotate-icon fa-2x"></i>
      </h3>
    </a>

  </div>

  <!-- Card body -->
  <div id="collapse10" class="collapse" role="tabpanel" aria-labelledby="heading10" data-parent="#accordionEx194">
    <div class="card-body pt-0">
      <p>We aim to deliver the best power solution to suit customer needs capable of delivering up to 1250KVA with the use of reliable power generator sets from variety of established manufacturers with a wide selection of generator engines as well as full generator customization, extended base tanks, frequency switches and remote PC indication and telemetry via GSM and Ethernet. Every power generator installation can be extended for service maintenance packages to allow smooth generator operation.</p>

      <h1 class="main-color">Outdoor Infrastructure</h1>
      <p>In view of providing efficient and effective service for the durability of our clients equipment that operates on diesel fuel; e.g. Diesel Generators, Diesel Vehicles, e.t.c and to increase its lifespan, we offers diesel bulk tank cleaning and management.</p>
      <p>This operation is been carried out in a highly professional manner by engaging our experienced team and observing every necessary operational health and safety measures to avoid every hazards associated to the scope of work.</p>
      <p>Cleaning of diesel bulk tank ensures effective running of the diesel operated machines by removal of dirt’s /contaminants in the diesel that can eventually cause the breakdown of the equipment. This activity offers significant cost savings through longer equipment life, greater efficiency and reduced time and money spent on maintenance. It also serves to extend our clients capital investments, by lowering the total lifecycle cost of the equipment associated with diesel fuel operations.</p>
    </div>
  </div>
  </div>
  <!-- Accordion card -->
  </div>
  <!--/.Accordion wrapper-->

  </div>


@endsection
