@extends('partials.uimain')


@section('content')


<div class="container space-top">
  <h2 class="text-center text-danger">Send us a Quote</h2>


      <!-- Default form contact -->
    <form class="text-center border border-light p-5">
      {{ csrf_field() }}
    <!-- Name -->
    <input type="text" name="full_name" class="form-control mb-4" placeholder="Full Name">

    <!-- Email -->
    <input type="email" name="email_address" class="form-control mb-4" placeholder="E-mail">
    <input type="text" class="form-control mb-4" placeholder="Telephone">

    <!-- Subject -->
    <label>Subject</label>
    <input type="text" name="subject" class="form-control mb-4" placeholder="Quote Subject">


    <!-- Message -->
    <div class="form-group">
    <textarea class="form-control rounded-0" name="quote_content" rows="7" placeholder="Enter your quote content here"></textarea>
    </div>

    <!-- Material checked -->
    <div class="form-check">
      <input type="checkbox" name="send_me_copy" class="form-check-input" id="materialChecked2" checked>
      <label class="form-check-label" for="materialChecked2">Send me a copy of quote ?</label>
    </div>
    <!-- Send button -->
    <button class="btn btn-info btn-block" type="submit">Send Quote</button>

    </form>
    <!-- Default form contact -->





  </div>


@endsection
