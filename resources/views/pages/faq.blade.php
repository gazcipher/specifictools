@extends('partials.uimain')





@section('content')


<div class="container space-top">
  <h1 class="text-center text-danger">Frequently Asked Questions (FAQ)</h1>

    <div class="container">
      <h4 class="text-justify">
        Below are some of the questions that our clients keep asking about our services and operations within and outside Nigeria. We advise that you take your time and go through them. Thank you for your interest in ST&T Ltd.
      </h4>
    </div>


                    <!--Accordion wrapper-->
                  <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

                  <!-- Accordion card -->
                  <div class="card">

                  <!-- Card header -->
                  <div class="card-header" role="tab" id="headingOne1">
                      <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
                      aria-controls="collapseOne1">
                      <h5 class="mb-0">
                        What is the full meaning of ST&T? <i class="fas fa-angle-down rotate-icon"></i>
                      </h5>
                      </a>
                  </div>

                  <!-- Card body -->
                  <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
                      <div class="card-body">
                      ST&T is an abbreviation for Specific Tools and Techniques Limited
                      </div>
                  </div>

                  </div>
                  <!-- Accordion card -->

                  <!-- Accordion card -->
                  <div class="card">

                  <!-- Card header -->
                  <div class="card-header" role="tab" id="headingTwo2">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
                    aria-expanded="false" aria-controls="collapseTwo2">
                    <h5 class="mb-0">
                      How do I Locate a ST&T branch near me? <i class="fas fa-angle-down rotate-icon"></i>
                    </h5>
                  </a>
                  </div>

                  <!-- Card body -->
                  <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2" data-parent="#accordionEx">
                      <div class="card-body">
                      Click <a href="{{ route('pages.contactus')}}">here</a> to get to our Contact Us Page. There you will find addresses of our Office Locations across Nigeria</p>
                      </div>
                  </div>

                  </div>
                  <!-- Accordion card -->

                  <!-- Accordion card -->
                  <div class="card">

                  <!-- Card header -->
                  <div class="card-header" role="tab" id="headingThree3">
                      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
                      aria-expanded="false" aria-controls="collapseThree3">
                      <h5 class="mb-0">
                        What are the toll free lines that I can call your center on? <i class="fas fa-angle-down rotate-icon"></i>
                      </h5>
                      </a>
                  </div>

                  <!-- Card body -->
                  <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3" data-parent="#accordionEx">
                      <div class="card-body">
                      +2348032023945, +23412911130
                      </div>
                  </div>

                  </div>
                  <!-- Accordion card -->
                  <!-- Accordion card -->
                  <div class="card">

                  <!-- Card header -->
                  <div class="card-header" role="tab" id="headingThree4">
                      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree4"
                      aria-expanded="false" aria-controls="collapseThree4">
                      <h5 class="mb-0">
                        What are the Core Values of ST&T? <i class="fas fa-angle-down rotate-icon"></i>
                      </h5>
                      </a>
                  </div>

                  <!-- Card body -->
                  <div id="collapseThree4" class="collapse" role="tabpanel" aria-labelledby="headingThree4" data-parent="#accordionEx">
                      <div class="card-body">
                      We are Customer Driven, Innovative, Respectful to individuals, and ensure Safety of Stakeholders.
                      </div>
                  </div>

                  </div>
                  <!-- Accordion card -->
                  <!-- Accordion card -->
                  <div class="card">

                  <!-- Card header -->
                  <div class="card-header" role="tab" id="headingThree5">
                      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree5"
                      aria-expanded="false" aria-controls="collapseThree5">
                      <h5 class="mb-0">
                        How costly are ST&T Services compared to others? <i class="fas fa-angle-down rotate-icon"></i>
                      </h5>
                      </a>
                  </div>

                  <!-- Card body -->
                  <div id="collapseThree5" class="collapse" role="tabpanel" aria-labelledby="headingThree5" data-parent="#accordionEx">
                      <div class="card-body">
                      Our services are comparatively cheaper in cost than most of our competitors despite maintenance of high ethical standards. We are able to render cheaper services because of our financial strength and many years of experience.
                      </div>
                  </div>

                  </div>
                  <!-- Accordion card -->
                  <!-- Accordion card -->
                  <div class="card">

                  <!-- Card header -->
                  <div class="card-header" role="tab" id="headingThree6">
                      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree6"
                      aria-expanded="false" aria-controls="collapseThree6">
                      <h5 class="mb-0">
                        What time does ST&T Open Office? <i class="fas fa-angle-down rotate-icon"></i>
                      </h5>
                      </a>
                  </div>

                  <!-- Card body -->
                  <div id="collapseThree6" class="collapse" role="tabpanel" aria-labelledby="headingThree6" data-parent="#accordionEx">
                      <div class="card-body">
                      Our offices nation wide open on Mondays to fridays between 8.00am to 6.00pm while weekend (Saturday) is 10.00am to 4.00pm.
                      </div>
                  </div>

                  </div>
                  <!-- Accordion card -->
                  <!-- Accordion card -->
                  <div class="card">

                  <!-- Card header -->
                  <div class="card-header" role="tab" id="headingThree7">
                      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree7"
                      aria-expanded="false" aria-controls="collapseThree7">
                      <h5 class="mb-0">
                        Does ST&T accept SIWES Students and Corps Members for NYSC? <i class="fas fa-angle-down rotate-icon"></i>
                      </h5>
                      </a>
                  </div>

                  <!-- Card body -->
                  <div id="collapseThree7" class="collapse" role="tabpanel" aria-labelledby="headingThree7" data-parent="#accordionEx">
                      <div class="card-body">
                      Students that have interest in our company are accepted for industrial training provided they come from relevant department. We also accept Corps Members for NYSC.
                      </div>
                  </div>

                  </div>
                  <!-- Accordion card -->
                  <!-- Accordion card -->
                  <div class="card">

                  <!-- Card header -->
                  <div class="card-header" role="tab" id="headingThree8">
                      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree8"
                      aria-expanded="false" aria-controls="collapseThree8">
                      <h5 class="mb-0">
                        Still Have Some Unanswered Questions? <i class="fas fa-angle-down rotate-icon"></i>
                      </h5>
                      </a>
                  </div>

                  <!-- Card body -->
                  <div id="collapseThree8" class="collapse" role="tabpanel" aria-labelledby="headingThree8" data-parent="#accordionEx">
                      <div class="card-body">
                      We are committed to Answering all your Questions. Please Click <a target="_blank" href="mailto:info@specifictoolsng.com">Here</a> to get a chance to send us any Question you need further answers to. We will get back to you as soon as possible.
                      </div>
                  </div>

                  </div>
                  <!-- Accordion card -->

                  </div>
                  <!-- Accordion wrapper -->


</div>




@endsection
