@extends('partials.uimain')


@section('content')


<div class="container space-top">
  <!--<h2 class="text-center text-danger">Welcome to our structure</h2> -->

  <!--Accordion wrapper-->
<div class="accordion md-accordion accordion-3 z-depth-1-half" id="accordionEx194" role="tablist"
aria-multiselectable="true">

<!-- <ul class="list-unstyled d-flex justify-content-center pt-5 red-text">
<li><i class="fas fa-anchor mr-3 fa-2x" aria-hidden="true"></i></li>
<li><i class="far fa-life-ring mr-3 fa-2x" aria-hidden="true"></i></li>
<li><i class="far fa-star fa-2x" aria-hidden="true"></i></li>
</ul> -->

<h2 class="text-center text-capitalized red-text py-4 px-3">Our Structure</h2>

<hr class="mb-0">

<!-- Accordion card -->
<div class="card">

<!-- Card header -->
<div class="card-header" role="tab" id="heading1">
  <a data-toggle="collapse" data-parent="#accordionEx194" href="#collapse1" aria-expanded="true"
    aria-controls="collapse1">
    <h3 class="mb-0 mt-3 red-text">
      Environment Health and Safety Plan <i class="fas fa-angle-down rotate-icon fa-2x"></i>
    </h3>
  </a>
</div>

<!-- Card body -->
<div id="collapse1" class="collapse show" role="tabpanel" aria-labelledby="heading1" data-parent="#accordionEx194">
  <div class="card-body pt-0">
    <p>
      Environmental Health and Safety are critical issues in our service and maintenance business. It is also an intrinsic aspect of the nature of the business carried out by most of our clients.
      <br/>
      The Company's environmental, health and safety policy covers safety of personnel, care of materials, safe handling and operation of equipment, safe-working habits in execution of services and environmental conciousness andawareness. Environment, Health and Safety
    </p>
    <h1 class="main-color">Affordability</h1>
    <p>
      Our services are offered at very competitive and attarctive prices
    </p>
  </div>
</div>
</div>
<!-- Accordion card -->

<!-- Accordion card -->
<div class="card">

<!-- Card header -->
<div class="card-header" role="tab" id="heading2">
  <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse2" aria-expanded="false"
    aria-controls="collapse2">
    <h3 class="mb-0 mt-3 red-text">
      Equipments and Spare Parts <i class="fas fa-angle-down rotate-icon fa-2x"></i>
    </h3>
  </a>
</div>

<!-- Card body -->
<div id="collapse2" class="collapse" role="tabpanel" aria-labelledby="heading2" data-parent="#accordionEx194">
  <div class="card-body pt-0">
  <p>
    Each region has complete set of special and general equipment and tools required to efficiently carry out their tasks. The Company also maintains stock level of key consumables and spare parts for each repair and maintenance segment.
  </p>
  </div>
</div>
</div>
<!-- Accordion card -->

<!-- Accordion card -->
<div class="card">

<!-- Card header -->
<div class="card-header" role="tab" id="heading3">
  <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse3" aria-expanded="false"
    aria-controls="collapse3">
    <h3 class="mb-0 mt-3 red-text">
      Experience & Expertise <i class="fas fa-angle-down rotate-icon fa-2x"></i>
    </h3>
  </a>
</div>

<!-- Card body -->
<div id="collapse3" class="collapse" role="tabpanel" aria-labelledby="heading3" data-parent="#accordionEx194">
  <div class="card-body pt-0">
    <p>
      Specific Tools & Techniques ST&T Ltd has attained a strong reputation in the servicing and maintenance of offices and site locations of several companies in Nigeria. The combined experience has been aquired through the businesses of the members that span several industries and in different communities from South-South, South-West and South-East.
    </p>
    <h1 class="main-color">Our Affiliations include:</h1>
    <ul>
      <li>International Facilities Management Association (IFMA)</li>
      <li>Nigeria Society of Engineers (MNSE)</li>
      <li>Institute of Chattered Accountants</li>
    </ul>
    <h1 class="main-color">License</h1>
    <ul>
      <li>National Communication Commission: NCC License for Co-Location Infrastructure Management</li>
    </ul>
    <p>...We look forward to a good business relationship with you</p>
  </div>
</div>
</div>
<!-- Accordion card -->
<!-- Accordion card -->
<div class="card">

<!-- Card header -->
<div class="card-header" role="tab" id="heading4">
  <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse4" aria-expanded="false"
    aria-controls="collapse4">
    <h3 class="mb-0 mt-3 red-text">
      Quality Assurance Plan <i class="fas fa-angle-down rotate-icon fa-2x"></i>
    </h3>
  </a>

</div>

<!-- Card body -->
<div id="collapse4" class="collapse" role="tabpanel" aria-labelledby="heading4" data-parent="#accordionEx194">
  <div class="card-body pt-0">
  <p>
    Our Quality and Assurance Plan ensures that Appropriate Insection Systems are Implemented. This involves the provision of checklists for record keeping activities that cover all Systems of Operations. Some of our Quality Assurance Activities are:
  </p>
    <ul>
      <li>Planning, Scheduling and Appraisal of Preventive Maintenance.</li>
      <li>Tracking Required Maintenance Services.</li>
      <li>Planned proactive and Reactive Inspections.</li>
      <li>Inspecting, Discovering, Evaluating, and Documenting Deficiencies.</li>
      <li>Monitoring of the Life of Equipments and Facilities.</li>
      <li>Developing a Maintenance File.</li>
    </ul>
  </div>
</div>
</div>
<!-- Accordion card -->
<div class="card">

<!-- Card header -->
<div class="card-header" role="tab" id="heading5">
  <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse5" aria-expanded="false"
    aria-controls="collapse5">
    <h3 class="mb-0 mt-3 red-text">
      Regional Structure Organogram <i class="fas fa-angle-down rotate-icon fa-2x"></i>
    </h3>
  </a>

</div>

<!-- Card body -->
<div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="heading5" data-parent="#accordionEx194">
  <div class="card-body pt-0">
      <div class="img-responsive">
        <img src="{{ asset('logos/regionalstruct.jpg') }}" alt="Regional Structure Chart">
      </div>
  </div>
</div>
</div>
<!-- Accordion card -->
<div class="card">

<!-- Card header -->
<div class="card-header" role="tab" id="heading6">
  <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse6" aria-expanded="false"
    aria-controls="collapse6">
    <h3 class="mb-0 mt-3 red-text">
      Financial Status and Turnover <i class="fas fa-angle-down rotate-icon fa-2x"></i>
    </h3>
  </a>

</div>

<!-- Card body -->
<div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="heading6" data-parent="#accordionEx194">
  <div class="card-body pt-0">
    <ul>
      <li>Current Financial Status of about ₦3 Billion annully.</li>
      <li>Asset Worth of about ₦350 Million.</li>
      <li>Company Turnover for the year 2014 and 2015 is ₦2.8b and ₦3.2b respectively.</li>
    </ul>
  </div>
</div>
</div>
<!-- Accordion card -->
<div class="card">

<!-- Card header -->
<div class="card-header" role="tab" id="heading7">
  <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse7" aria-expanded="false"
    aria-controls="collapse7">
    <h3 class="mb-0 mt-3 red-text">
      Structure People Growth <i class="fas fa-angle-down rotate-icon fa-2x"></i>
    </h3>
  </a>

</div>

<!-- Card body -->
<div id="collapse7" class="collapse" role="tabpanel" aria-labelledby="heading7" data-parent="#accordionEx194">
  <div class="card-body pt-0">

    <h1 class="main-color">ST&T People Growth Structure</h1>
    <ul>
      <li>Each Staff Responsibilities is defined in line with the Executive Business Plan</li>
      <li>Staff are adequately Motivated and Periodically Appraised and Compensated</li>
      <li>Staff Capacity Building is Continous</li>
      <li>In Areas of Incompetence, Services are Out Sourced</li>
      <li>Implementation of the National Health Insurance Scheme (NHIS)</li>
      <li>Strict Adherence to Occupational Health, Safety and Environment (HSE) Policies</li>
      <li>Trainings</li>
    </ul>
  </div>
</div>
</div>
<!-- Accordion card -->
</div>
<!--/.Accordion wrapper-->
</div>


@endsection
