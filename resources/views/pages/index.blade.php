@extends('partials.uimain')

@section('title', 'Welcome to Specific')

@section('stylesheets')

  <style>
    #mySidenav a {
      position: absolute;
      left: -80px;
      transition: 0.3s;
      padding-top:15px;
      padding-left: 10px;
      width: 150px;
      text-decoration: none;
      font-size: 20px;
      height: 150px;
      color: white;
      border-radius: 5px 5px 0 0;
      transform:rotate(90deg);

    }

    #mySidenav a:hover {
      left: 0;
    }

    #quote-request {
      top: 700px;
      background: #990000 !important;
      color: #fff;
    }
    .vertical-text {
    	transform: rotate(90deg);
    	transform-origin: left top 0;
    }

    /* #blog {
      top: 80px;
      background-color: #2196F3;
    }

    #projects {
      top: 140px;
      background-color: #f44336;
    }

    #contact {
      top: 200px;
      background-color: #555
    } */
</style>





@endsection

@section('content')

  <div id="carouselExampleIndicators" class="wrap carousel slide" data-ride="carousel" style="margin-top:7px;">
      <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="img-fluid d-block w-100" src="{{ asset('logos/slider1.JPG') }}" alt="First slide" style="max-height:520px;min-height:520px;">
            <div class="carousel-caption d-none d-md-block">
              <h5>Specific Tools NG</h5>
              <!--<p>Launching from the North-South pad</p>-->
            </div>
          </div>
      <div class="carousel-item">
          <img class="img-fluid d-block w-100" src="{{ asset('logos/slider2.JPG') }}" alt="Second slide" style="max-height:520px;min-height:520px;">
          <div class="carousel-caption d-none d-md-block">
            <h5>Stable Power &</h5>
            <p>Reliable Economy</p>
          </div>
      </div>
      <div class="carousel-item">
          <img class="img-fluid d-block w-100" src="https://www.developeronrent.com/blogs/wp-content/uploads/2018/08/Serverless-Computing-tools-810x405.png" alt="Third slide" style="max-height:520px;min-height:520px;">
          <div class="carousel-caption d-none d-md-block">
            <h5>Rocket Launch Pad 103</h5>
            <p>Launching from the south-west pad</p>
          </div>
      </div>
      <div class="carousel-item">
          <img class="img-fluid d-block w-100" src="https://i.kinja-img.com/gawker-media/image/upload/s--faG4Y4XP--/c_scale,f_auto,fl_progressive,q_80,w_800/jv6zzo15tzuitdfbcotg.jpg" alt="Fourt slide" style="max-height:520px;min-height:520px;">
          <div class="carousel-caption d-none d-md-block">
            <h5>Rocket Launch Pad 104</h5>
            <p>Launching from the South-East Pad</p>
          </div>
      </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
      </a>
  </div>

  {{-- Hoverable Side Bar --}}
  <div id="mySidenav" class="sidenav">
  <a href="{{ route('pages.quote') }}" id="quote-request">Get a Quote  <i class="fa fa-comments-o"></i> </a>
  <!--
  <a href="{{ route('pages.quote') }}" id="quote-request">
    <img src="{{ asset('logos/get_a_quote_button.jpg')}}" alt="Get a Quote Link button" style="max-width:120px;max-height:100px;">
  <a href="#" id="blog">Blog</a>
  <a href="#" id="projects">Projects</a>
  <a href="#" id="contact">Contact</a> -->
</div>

  <!--Middle Content -->
<div class="jumbotron">
<div class="container">
    <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-4">
          <div class="card middle-content-card-height header-color">
            <div class="card-body">
              <h5 class="card-title">Tool 1</h5>
              <p class="card-text">Tool descriptions. With supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
          </div>
        </div>

        <div class="col-sm-4 col-md-4 col-lg-4">
          <div class="card middle-content-card-height header-color">
            <div class="card-body">
              <h5 class="card-title">Tool 2</h5>
              <p class="card-text">Tool Descriptions. With supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
          </div>
        </div>

        <div class="col-sm-4 col-md-4 col-lg-4">
          <div class="card middle-content-card-height header-color">
            <div class="card-body">
              <h5 class="card-title">Tool 3</h5>
              <p class="card-text">Tool 3 Descriptions. With supporting text below as a natural lead-in to additional content. kdk kdk kdkd kd kk kdkk kdk kdkd kkdk k kdk kdk kdk kdk k e</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
{{--
  <div class="container services-header2">
        <!--<h2 class="text-center main-color">Video Message</h2>-->
        <hr class="video-border"/>
        <div class="col-12" style="min-height:300px !important;margin-top:7px !important;">
          <div id="player-overlay">
            <video height="400" controls autoplay>
              <source src="{{URL::asset('videos/Africa_is_the_youngest_continent.mp4')}}" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>
        </div>
  </div>--}}

  <!-- Our Services Slider, Three item per row -->

  <div class="jumbotron">
  <div class="container services-header2 generic-pull-top" style="background:#fff !important;">
    <div class="container" style="min-height:100px;">
      <h2 class="text-center main-color"> <br/>Our Services</h2>
    </div>
    <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-4">
          <div class="card middle-content-card-height header-color">
            <div class="card-body">
              <h5 class="card-title">Turnkey Supply & Deployment of Telecom Infrastructure</h5>
              <!--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>-->
              <a href="{{ url('services')}}#heading4" class="btn btn-primary">Read More</a>
            </div>
          </div>
        </div>

        <div class="col-sm-4 col-md-4 col-lg-4">
          <div class="card middle-content-card-height header-color">
            <div class="card-body">
              <h5 class="card-title">Operations and Maintenance of Telecom Infrastructure</h5>
              <!--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>-->
              <a href="{{ url('services')}}#heading5" class="btn btn-primary">Read More</a>
            </div>
          </div>
        </div>

        <div class="col-sm-4 col-md-4 col-lg-4">
          <div class="card middle-content-card-height header-color">
            <div class="card-body">
              <h5 class="card-title">Integrated Facilities Management & Maintenanc</h5>
            <!--  <p class="card-text">With supporting text below as a natural lead-in to additional content. kdk kdk kdkd kd kk kdkk kdk kdkd kkdk k kdk kdk kdk kdk k e</p>-->
              <a href="{{ url('services')}}#heading6" class="btn btn-primary">Read More</a>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

  <div class="container">
      <h1 class="text-center main-color">Our Clients</h1>
        @include('sliders.smallcarousel')
  </div>



  <div class="container-fluid generic-header">
    <h2 class="text-center space-top"><br/>Resources</h2>
  </div>
  <div class="container-fluid footer-content">
      <h2 class="text-center"><br/>Quick Access</h2>
      <hr class="hr-line-color">
      <div class="row offset-2 space-top">
        <div class="col-4">
          <h2>Who We Are</h2>
          <hr class="hr-line-color">
          <h4><a class="white-color" href="{{ route('pages.aboutus') }}">Read About Us</a></h4>
        </div>
        <div class="col-4 footer-font">
          <h2>Thrending News</h2>
          <hr class="hr-line-color">
          @foreach($news as $trendingNews)
          <li style="font-family:lato !important;"><a style="font-family:lato !important;" class="white-color" href="{{ route('pages.single', $trendingNews->news_slug)}}"> {{$trendingNews->title }}</a></li>
          @endforeach
        </div>
        <div style="font-family:lato !important;" class="col-4" id="our-contact">
          <h2>Address:</h2>
          <hr class="hr-line-color">
          <address class="footer-font" style="font-family:lato !important;">
            51B Adekunle Bisi Street, <br/>Hill View Estate Ogudu GRA, Ojota, Lagos.
            <br/>
            Specific Tools NG <a class="white-color" href="mailto:info@specifictoolsng">Send Us Email</a><br>
          </address>
        </div>

      </div>
  </div>

@endsection
