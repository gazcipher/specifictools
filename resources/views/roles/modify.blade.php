@extends('partials.uimain')

@section('title', '| Edit Role')


@section('stylesheets')

    {{--Load page specific stylesheets that will only affect this page --}}


@endsection

@section('content')

    <div class="row space-top">

      <div class="col-6 offset-3">
        {{ Form::model($role, ['route' => ['roles.update', $role->id], 'method' => 'PUT']) }}
          <br/>          <br/>          <br/>
          <h4 class="text-center text-primary">Edit Role</h3>
          {{ Form::text('name', null, ['class' => 'form-control form-spacing', 'placeholder' => 'Role Name'])}}
          {{ Form::textarea('role_description', null, ['class' => 'form-control form-spacing', 'placeholder' => 'Role Description'])}}
          {{ Form::submit('Save Changes', ['class' => 'btn btn-primary btn-block form-spacing'])}}

        {!! Form::close() !!}
      </div>
      <div class="col-4 offset-4">
          {!! Html::linkRoute('roles.index', 'Cancel', array('class' => 'btn btn-danger btn-block')) !!}
      </div>

    </div>


@endsection
