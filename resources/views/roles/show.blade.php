@extends('partials.uimain')

@section('title', '| Role and Users')


@section('stylesheets')

    {{--Load page specific stylesheets that will only affect this page --}}

@endsection

@section('content')

  <div class="row space-top">
    <div class="col-8 offset-2">
      <h2>{{ $role->name }} Role <small>{{$role->users()->count() }} Users</small></h2>
    </div>
    <div class="col-md-2">
      <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-sm btn-primary pull-right btn-block" style="margin-top:20px;">Edit</a>
    </div>
    <div class="col-md-2">
      {{ Form::open(['route' => ['roles.destroy', $role->id], 'method' => 'DELETE']) }}
        {{ Form::submit('Delete Role', ['class' => 'btn btn-danger btn-block', 'style' => 'margin-top:20px;']) }}
      {{ Form::close() }}
    </div>
  </div>

    <div class="row">
      <div class="col-md-12">
        <table class="table">
          <thead>
            <tr>
              <th>#</th>
              <th>User</th>
              <th>Role(s) | Permission(s)</th>
              <th></th>
            </tr>
          </thead>
            <tbody>
              @foreach($role->users as $user)
                <tr>
                  <th>{{ $user->id }}</th>
                  <td>{{ $user->othernames }}</td>
                  <td>
                    @foreach($user->roles as $role)
                      <span class="label label-default">{{ $role->name }}</span>
                    @endforeach
                  </td>
                  <td><a href="{{ route('users.show', $user->id) }}" class="btn btn-xs btn-default">View</a></td>
                </tr>
              @endforeach
            </tbody>
        </div>
      </div>
    </div>




@endsection
