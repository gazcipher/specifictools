@extends('partials.uimain')

@section('title', '| Available Roles')


@section('stylesheets')

    {{--Load page specific stylesheets that will only affect this page --}}


@endsection

@section('content')

  <div class="row space-top">
    <div class="col-6 offset-2">
      <h2 class="text-center">Roles | Permissions</h2>
      <table class="table table-condensed">
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
            @foreach($roles as $role)
            <tr>
              <td><a href="{{ route('roles.show', $role->id)}}"> {{ $role->name }} </a></td>
              <td><a href="{{ route('roles.show', $role->id)}}"> {{ $role->role_description }} </a></td>
              <td>
                <a class="btn btn-primary btn-sm" href="{{route('roles.edit', $role->id)}}"><i class="fa fa-pencil"></i> Edit</a>
        {{--        {!! Form::open(['route' => ['roles.destroy', $role->id], 'method' => 'DELETE']) !!}
                  {!! Form::submit('Delete', array('class' => 'btn btn-danger btn-sm')) !!}
                {!! Form::close() !!}     --}}
              </td>
            </tr>
            @endforeach
        </tbody>
      </table>
    </div>

{{--    <div class="col-md-2"> <!--Form for creating category -->
      <div class="well">
        {!! Form::open(['route' => 'roles.store', 'method' => 'POST']) !!}
                  <br/>          <br/>          <br/>
          <h4 class="text-center text-primary">Create New Role</h3>

          {{ Form::text('name', null, ['class' => 'form-control form-spacing', 'placeholder' => 'Role Name'])}}
          {{ Form::textarea('role_description', null, ['class' => 'form-control form-spacing', 'placeholder' => 'Role Description'])}}
          {{ Form::submit('Add Role', ['class' => 'btn btn-primary btn-block form-spacing'])}}

        {!! Form::close() !!}
      </div>
    </div>--}}
  </div>

@endsection
