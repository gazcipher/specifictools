
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- CSRF TOKEN PROTECTION --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Specific Tools, Power, Mast and Network Installation and Maintenance!')</title>


    {{-- MDB Javascript and JQuery CDN --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.3/css/mdb.min.css" rel="stylesheet">

    <!-- Fonts -->
   <link rel="dns-prefetch" href="//fonts.gstatic.com">
   <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
   <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">

    {{-- Font Awesome --}}
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">



    {{-- cdn and libraries for the NAV Menu --}}
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">
    {{-- Html::style('css/lib/bootstrap/css/bootstrap.min.css') --}}
    {{-- Html::style('css/lib/font-awesome/css/font-awesome.min.css') --}}
    {{-- Html::style('css/lib/animate/animate.min.css') --}}
    {{-- Html::style('css/lib/ionicons/css/ionicons.min.css') --}}
    {{-- Html::style('css/lib/owlcarousel/assets/owl.carousel.min.css') --}}
    {{-- Html::style('css/lib/magnific-popup/magnific-popup.css') --}}
    {{-- Html::style('css/lib/ionicons/css/ionicons.min.css') --}}
    {{ Html::style('css/style.css') }}





    {{ Html::style('css/main.css') }}

    {{-- Load page specific stylesheets --}}
    @yield('stylesheets')
