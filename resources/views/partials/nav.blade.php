<header>
			<div id="navbar" class="container-fluid nav-height">
				<div class="container nav-fill w-100">
					<nav class="navbar navbar-expand-md navbar-light" role="navigation">
						<button class="navbar-toggler" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarContent">
							<ul class="navbar-nav nav-fill w-100">
                <li class="nav-item">
                  <a href="{{ url('/') }}" role="button" class="btn btn-default"><i class="material-icons md-48">home</i></a>
                </li>
								<li class="nav-item">
									<div class="btn-group">
									  <a href="{{ route('pages.aboutus') }}" role="button" class="btn btn-default">About Us</a>
									  {{-- <button class="btn btn-default dropdown-toggle dropdown-toggle-split d-none d-md-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									    <span class="sr-only">Toggle Dropdown</span>
									  </button>
									  <div class="dropdown-menu">
											<a class="dropdown-item" href="#">Item</a>
									  </div> --}}
									</div>
								</li>
								<li class="nav-item">
									<div class="btn-group">
									  <a href="#" role="button" class="btn btn-default">Our Services</a>
									  <button class="btn btn-default dropdown-toggle dropdown-toggle-split d-none d-md-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									    <span class="sr-only">Toggle Dropdown</span>
									  </button>
									  <div class="dropdown-menu">
											<a class="dropdown-item" href="#">Service 1</a>
											<a class="dropdown-item" href="#">Service 2</a>
									  </div>
									</div>
								</li>
								<li class="nav-item">
									<div class="btn-group">
									  <a href="{{ route('pages.structure') }}" role="button" class="btn btn-default">Our Structure</a>
									</div>
								</li>
								<li class="nav-item">
									<div class="btn-group">
									  <a href="{{ route('pages.contactus') }}" role="button" class="btn btn-default">Contact Us</a>
									</div>
								</li>
								<li class="nav-item">
									<div class="btn-group">
									  <a href="{{ route('pages.faq') }}" role="button" class="btn btn-default">FAQ</a>
									</div>
								</li>
								<li class="nav-item">
									<div class="btn-group">
									  <a href="{{ route('news.index') }}" role="button" class="btn btn-default">News</a>
									</div>
								</li>
								<li class="nav-item">
									<div class="btn-group">
									  <a href="{{ route('login') }}" role="button" class="btn btn-default">Login</a>
									</div>
								</li>
								<li class="nav-item">
									<div class="btn-group">
									  <a href="{{ route('register') }}" role="button" class="btn btn-default">Register</a>
									</div>
								</li>

							</ul>
						</div>
					</nav>
				</div>
			</div>
		</header>
