{{-- MDB Javascript and JQuery CDN --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
{{-- Bootstrap tooltips --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
{{-- Bootstrap core JavaScript --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>
{{-- MDB core JavaScript --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.3/js/mdb.min.js"></script>


<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>



{{-- JavaScript Libraries --}}
{{ Html::script('js/lib/jquery/jquery.min.js') }}
{{ Html::script('js/lib/jquery/jquery-migrate.min.js') }}
{{ Html::script('js/lib/bootstrap/js/bootstrap.bundle.min.js') }}
{{ Html::script('js/lib/easing/easing.min.js') }}
{{ Html::script('js/lib/superfish/hoverIntent.js') }}
{{ Html::script('js/lib/superfish/superfish.min.js') }}
{{ Html::script('js/lib/wow/wow.min.js') }}
{{ Html::script('js/lib/owlcarousel/owl.carousel.min.js') }}
{{ Html::script('js/lib/magnific-popup/magnific-popup.min.js') }}
{{ Html::script('js/lib/sticky/sticky.js') }}
{{ Html::script('js/main.js') }}
