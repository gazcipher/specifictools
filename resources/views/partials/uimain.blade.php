<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

   {{-- including partial --}}
  @include('partials.header')

  <!--Add page specific stylesheets here-->

</head>

<body>

    @include('partials.navigation')

    <div class="well">

        @include('partials.messages') <!--error messages -->

          @yield('content')


    </div>

      <hr/>
      @include('partials.footer')
</div>


    @include('partials.scripts')

    {{--Load specific javascript for pages --}}
    @yield('javascripts')
</body>
</html>
