<div class="container">

	{{-- =================Top Navigation or social media icon=========================== --}}
	<section id="topbar" class="d-none d-lg-block">
	    <div class="container clearfix">
	    {{--  <div class="contact-info float-left">
	        <i class="fa fa-envelope-o"></i> <a href="mailto:info@specifictoolsng.com">info@specifictoolsng.com</a>
	        <i class="fa fa-phone"></i> +23480-000-0000
	      </div>--}}

	      <div class="social-links float-right">
	        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
	        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
	        <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
	        <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
	        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
	      </div>
	    </div>
	  </section>
	  {{-- =====================End top nav for social media icon============================== --}}

	<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
	<center>
		&copy; 2018 -
	<script type="text/javascript">
		var year = new Date().getFullYear();
		document.write(year);
	</script>
		Specific Tools NG | All Rights Reserved.
	</center>

</div>
