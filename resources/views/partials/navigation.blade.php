{{-- =================Top Navigation or social media icon=========================== --}}
<section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="fa fa-envelope-o"></i> <a href="mailto:info@specifictoolsng.com">info@specifictoolsng.com</a>
        <i class="fa fa-phone"></i> +23480-000-0000
      </div>
      <div class="social-links float-right">
      {{--  <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
        <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>--}}
      </div>
    </div>
  </section>
  {{-- =====================End top nav for social media icon============================== --}}

  {{-- ===============================Menu - main navigation=============================== --}}
  <header id="header">
   <div class="container">

     <div id="logo" class="pull-left">
        <!-- Uncomment below if you prefer to use an image logo -->
      <a href="{{ url('/') }}"><img class="logo-pullup" src="{{ asset('logos/FullLogoWhite.png') }}" alt="Specific Tools Logo" /></a>
     </div>

     <nav id="nav-menu-container">
       <ul class="nav-menu">
         <li class="menu-active"><a href="{{ url('/')}}"><img style="max-width:35px;max-height:35px;" src="http://motoraty-media.s3-us-west-2.amazonaws.com/news/wp-content/uploads/2016/10/motoraty-home-page-icon-red.png" alt="Home iCON"> </a></li>
         <li class="menu-has-children"><a href="{{ route('pages.aboutus') }}">About Us</a>
              <ul>
                <li><a href="{{ route('pages.leadership') }}">Leadership Team</a></li>
              </ul>
            </li>
         <li><a href="{{ route('pages.structure') }}">Our Structure</a></li>
         <li><a href="{{ route('pages.service') }}">Services</a></li>

      <!--   <li class="menu-has-children"><a href="{{ route('pages.service') }}">Leadership Team</a>
           <ul>
             <li><a href="#">Service 1</a></li>
             <li><a href="#">Service 2</a></li>
             <li><a href="#">Service 3</a></li>
           </ul>
         </li>-->

         {{-- <li><a href="{{ url('/') }}#our-contact">Contact</a></li> --}} <!-- ==================How to link route to ID tag on a div -->
         <li><a href="{{ route('pages.contactus') }}">Contact</a></li>
         <li><a href="{{ route('news.index') }}">News</a></li>
         <li><a href="{{ route('pages.faq')}}">FAQ</a></li>

      {{--   <li class="menu-has-children"><a href="#">Staff Portal</a>
            <ul>
              @guest
                  <li class="nav-item">
                      <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                  </li>
                  @if (Route::has('register'))
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                      </li>
                  @endif
            </ul>
          </li>--}}
        @guest
        <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>

         @else
         <li><a href="{{ route('news.index') }}">{{ __('All News') }}</a></li>

         @if(Auth::user()->roles("Administrator"))
         <li><a href="{{ route('news.create') }}">{{ __('Add News') }}</a></li>
         <li><a href="{{ route('roles.index') }}">{{ __('Roles | Access') }}</a></li>
         <li><a href="{{ route('users.index') }}">{{ __('Users | Access Control') }}</a></li>
         @endif
         <li class="menu-has-children"><a href="#">{{ Auth::user()->othernames }} {{ Auth::user()->surname }} <span class="caret"></span></a>
           <ul>
             <li>
               <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>

            </li>
           </ul>
         </li>
         @endguest


       </ul>
     </nav>
   </div>
 </header>
 {{-- ======================end main navigation================================= --}}
