@extends('partials.uimain')

@section('title', 'Edit News Post')

{{--Load stylesheet that will only affect this page --}}
@section('stylesheets')

	{!! Html::style('css/parsley.css') !!}

	<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
	<script type="text/javascript">
		tinymce.init({
			selector: 'textarea',
			plugins: 'link code',
			menubar: true //disable the menu bar if you dont want, set it to false or true
			// menu: {
			// 	view: {title: 'Edit', items: 'cut, copy, paste'}
			// }//customizing the menu bar
		});
	</script>

@endsection

@section('content')

	<div class="container space-top">
			<h3 class="text-center">Add News Post</h3>

			{!! Form::model($news, ['route' => ['news.update', $news->id], 'data-parsley-validate' => '', 'method' => 'PUT', 'files' => true]) !!}


				{{ Form::label('title', 'News Title:') }}
				{{ Form::text('title', null, array('class' => 'form-control form-control-md', 'placeholder' => 'News Title', 'required' => '', 'maxlength' => '120')) }}

				{{ Form::label('news_slug', 'News Link:', ['class' => 'form-spacing'])}}
				{{ Form::text('news_slug', null, array('class' => 'form-control form-control-md', 'placeholder' => 'Enter your news link eg. news-link-to-a-news', 'required' => '', 'min-length' =>'5', 'maxlength' => '255')) }}


				{{ Form::select('category_id', $categories, null, ['class' => 'form-control form-control-md form-spacing', 'required'])}}

				{{ Form::label('image_url', 'Upload Image:', ['class' => 'form-spacing']) }}
				{{ Form::file('image_url') }}

					<br/>

				{{ Form::label('body', 'Message:', ['class' => 'form-spacing'])}}
				{{ Form::textarea('body', null, array('class' => 'form-control', 'placeholder' => 'Enter your news message')) }}

				{{ Form::submit('Update News', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top:20px;')) }}


			{!! Form::close() !!}

		</div>
@endsection



@section('scripts')

	{!! Html::script('js/parsley.min.js') !!}

@endsection
