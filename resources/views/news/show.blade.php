@extends('partials.uimain')

<?php $titleTag = htmlspecialchars($news->title); ?>
@section('title', "| $titleTag ")

@section('content')

	<div class="row space-top">
		<div class="col-6 offset-2">
			<img src="{{ asset('news_images/' .$news->image_url) }}" alt="News Image">
			<h2>{{ $news->title }}</h2>
			<p class="lead">{!! $news->body !!}</p><!-- We use this braces to output the html, ensure it is totally save before using this curly brace and excalamation-->
			<hr>

			<!--display post comments -->

		</div>

		<div class="col-2">
			<div class="well">

				<dl class="dl-horizontal">
					<label>Post URL </label>
					<p><a href="{{ route('pages.single', $news->news_slug) }}">{{ route('pages.single', $news->news_slug) }}</a></p>
				</dl>

				<dl class="dl-horizontal">
					<label>Category: </label>
					<p>{{ $news->category->categoryName }}</p>
				</dl>

				<dl class="dl-horizontal">
					<label>Created On: </label>
					<p>{{ date('M j, Y h:i a', strtotime($news->created_at)) }}</p>
				</dl>

				<dl class="dl-horizontal">
					<label>Last Updated: </label>
					<p>{!! date('M j, Y h:i a', strtotime($news->updated_at)) !!} </p>
				</dl>

				<hr/>
				<div class="row">
					<div class="col-6">
						{!! Html::linkRoute('news.edit', 'Edit News', array($news->id), array('class' => 'btn btn-primary btn-block')) !!}
					</div>
					<div class="col-6">
						{!! Form::open(['route' => ['news.destroy', $news->id], 'method' => 'DELETE']) !!}

						{!! Form::submit('Delete Post', array('class' => 'btn btn-danger btn-block')) !!}
						{!! Form::close() !!}

					</div>
				</div>

				<!--View All Post button -->
				<hr/>
				<div class="row">
					<div class="col-12">
						{{ Html::linkRoute('news.index', '<< Show All News', [], ['class' => 'btn btn-default btn-block btn-h1-spacing']) }}
					</div>
				</div>


			</div>

		</div>
	</div>





@endsection
