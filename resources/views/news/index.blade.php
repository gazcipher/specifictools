@extends('partials.uimain')

@section('title', '| Trending News')


@section('stylesheets')

    {{--Load page specific stylesheets that will only affect this page --}}

@endsection



@section('content')

	<div class="row space-top">
		<div class="col-8 offset-2">
			<h2 class="text-danger">Threeding News</h2>
		</div>
	</div>

	@foreach($news as $newsPost)
	<div class="row space-top">
		<div class="col-8 offset-2">
			<h2>{{ $newsPost->title }}</h2>
			<h5><caption>Published: {{ date('M j, Y', strtotime($newsPost->created_at)) }}</caption></h5>

			<p>{{ substr(strip_tags($newsPost->body), 0, 160) }} {{ strlen(strip_tags($newsPost->body)) > 160 ? '...' : '' }}</p>
			<a href="{{ route('pages.single', $newsPost->news_slug)}}" class="btn btn-primary btn-sm">...Read More</a>

			<hr/>
		</div>

	</div>
	@endforeach

	<!--paginate link -->
	<div>
		<div class="col-6 offset-4">
			<div class="text-center">
				{!! $news->links() !!}
			</div>
		</div>
	</div>



@endsection
