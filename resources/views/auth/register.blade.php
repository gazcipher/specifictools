@extends('partials.uimain')

@section('content')
      <div class="container space-top">

        <!-- Material form register -->
        <div class="card">

        <h5 class="card-header info-color white-text text-center py-3">
        <strong>Sign up</strong>
        </h5>

        <!--Card content-->
        <div class="card-body px-lg-5 pt-0">

        <!-- Form -->
        <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}" class="text-center">
                                  @csrf
        <div class="form-row">
            <div class="col">
                <!-- First name -->
                <div class="md-form">
                    <input type="text" id="materialRegisterFormFirstName" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname') }}" required autofocus>
                    <label for="materialRegisterFormFirstName">Surname</label>
                    @if ($errors->has('surname'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('surname') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="col">
                <!-- Last name -->
                <div class="md-form">
                    <input type="text" id="materialRegisterFormLastName" class="form-control{{ $errors->has('othernames') ? ' is-invalid' : '' }}" name="othernames" value="{{ old('othernames') }}" required autofocus>
                    <label for="materialRegisterFormLastName">Other Names</label>
                    @if ($errors->has('othernames'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('othernames') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>

        <!-- E-mail -->
        <div class="md-form mt-0">
            <input type="email" id="materialRegisterFormEmail" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
            <label for="materialRegisterFormEmail">E-mail</label>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <!-- Password -->
        <div class="md-form">
            <input type="password" id="materialRegisterFormPassword" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required aria-describedby="materialRegisterFormPasswordHelpBlock">
            <label for="materialRegisterFormPassword">Password</label>
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            <small id="materialRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
                At least 8 characters and 1 digit
            </small>
        </div>
        <div class="md-form">
            <input type="password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required aria-describedby="materialRegisterFormConfirmPasswordHelpBlock">
            <label for="password-confirm">Confirm Password</label>
            <small id="materialRegisterFormConfirmPasswordHelpBlock" class="form-text text-muted mb-4">
                At least 8 characters and 1 digit
            </small>
        </div>

        <!-- Phone number -->
        <div class="md-form">
            <input type="text" id="materialRegisterFormPhone" class="form-control" name="mobile" aria-describedby="materialRegisterFormPhoneHelpBlock">
            <label for="materialRegisterFormPhone">Phone Number</label>
            <small id="materialRegisterFormPhoneHelpBlock" class="form-text text-muted mb-4">
                Optional - for two step authentication
            </small>
        </div>

        <!-- Sign up button -->
        <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">Register</button>



        <hr>
        <!-- Terms of service -->
        <p>By clicking
            <em>Sign up</em> you agree to our
            <a href="" target="_blank">terms of service</a>
          </p>
        </form>
        <!-- Form -->

        </div>

        </div>
        <!-- Material form register -->

      </div>
@endsection
