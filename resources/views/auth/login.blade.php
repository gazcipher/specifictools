@extends('partials.uimain')

@section('title', 'Sign In')

@section('content')

        <div class="container space-top">
            <!-- Material form login -->
            <div class="card">

            <h5 class="card-header info-color white-text text-center py-3">
            <strong>{{ __('Log in to your account') }}</strong>
            </h5>
            <!--Card content-->
            <div class="card-body px-lg-5 pt-0">
            <!-- Form -->
            <form method="POST" action="{{ route('login') }}" class="text-center">
              @csrf
            <!-- Email -->
            <div class="col-10">
              <div class="md-form">
                <input type="email" id="materialLoginFormEmail" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                <label for="materialLoginFormEmail">E-mail</label>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <!-- Password -->
            <div class="col-10">
              <div class="md-form">
                <input type="password" id="materialLoginFormPassword" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                <label for="materialLoginFormPassword">Password</label>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <div class="d-flex justify-content-around">
              <div>
                <!-- Remember me -->
                <div class="form-check">
                  <input type="checkbox" class="form-check-input" id="materialLoginFormRemember" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                  <label class="form-check-label" for="materialLoginFormRemember">Remember me</label>
                </div>
              </div>
              <div>
                <!-- Forgot password -->
                @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                    </a>
                @endif
              </div>
            </div>

            <!-- Sign in button -->
            <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit" name="commit" tabindex="3" type="submit">Sign In</button>

            <!-- Register -->
            <p>Not a member?
              <a href="{{ route('register') }}">Register</a>
            </p>
            </form>
            <!-- Form -->

            </div>

            </div>
            <!-- Material form login -->
        </div>



@endsection
