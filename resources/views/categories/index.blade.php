@extends('partials.uimain')

@section('title', '| Available News Categories')


@section('stylesheets')

    {{--Load page specific stylesheets that will only affect this page --}}

    {!! Html::style('css/parsley.css') !!}
	  {!! Html::style('css/select2.min.css') !!}



@endsection



@section('content')

  <div class="card">
    <div class="card-title">
      <h4 class="text-center">Available Movie Categories</h4>
    </div>
    <div class="card-body">
      <div class="table-responsive">
      <div class="col-md-5 col-md-offset-3">
          <table class="table mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shaddow--2dp customTable">
            <thead>
            <th>Category</th>
            <th>Description</th>
            <th>
              Actions
            </th>
        </thead>

        <tbody>
          @foreach($categories as $category)
          <tr>
            <td>{{ $category->categoryName }}</td>
            <td>{{ $category->description }}</td>
            <td>
              <a href="{{ route('categories.show', $category->id)}}" class="btn btn-primary btn-sm">View</a>
              <a href="{{ route('categories.show', $category->id)}}" class="btn btn-sm btn-warning">Suspend</a>
              <a href="{{ route('categories.destroy', $category->id)}}" class="btn btn-sm btn-danger">Delete</a>

            </td>
          </tr>
          @endforeach
        </tbody>
        </table>
      </div>

      <div class="col-md-3 pull-right">
          <div class="well">

            <h4 class="text-center">Add New Category</h4>

        {!! Form::open(array('route' => 'categories.store', 'data-parsley-validate' => '', 'method' => 'post')) !!}

          {{ Form::text('categoryName', null, array('class' => 'form-control', 'placeholder' => 'Category Name', 'required' => '', 'maxlength' => '120')) }}
          {{ Form::textarea('description', null, array('class' => 'form-control form-spacing', 'placeholder' => 'Enter Descrption for this Category', 'data-parsley-trigger' => 'change', 'maxlength' => '1000')) }}

          {{ Form::submit('Create Category', array('class' => 'btn btn-success btn-sm btn-block btn-top-spacing')) }}


      {!! Form::close() !!}
    </div>
  </div>
</div>
</div>
</div>



@endsection


@section('scripts')

    {{--Load Page specific script that will only affect this page --}}

    {!! Html::script('js/parsley.min.js') !!}
	  {!! Html::script('js/select2.min.js') !!}

	<script type="text/javascript">
			$('.select2-multi-tags').select2();
	</script>

@endsection
