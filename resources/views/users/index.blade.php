@extends('partials.uimain')

@section('title', '| Users')


@section('stylesheets')

    {{--Load page specific stylesheets that will only affect this page --}}


@endsection

@section('content')

  <div class="row space-top">
    <div class="col-6 offset-2">
      <h2 class="text-center">Users | Permissions</h2>
      <table class="table table-condensed">
        <thead>
          <tr>
            <th>Full Name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Permission(s)</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
              <td><a href="{{ route('users.show', $user->id)}}"> {{ $user->othernames }} {{ $user->surname}}</a></td>
              <td><a href="{{ route('users.show', $user->id)}}"> {{ $user->email }} </a></td>
              <td><a href="{{ route('users.show', $user->id)}}"> {{ $user->mobile }} </a></td>
              <td><a href="#">
                @foreach($user->roles as $role)
                  <li class="label label-default">{{ $role->name }}</li>
                @endforeach
              </a></td>
              <td>
                <a class="btn btn-primary btn-sm" href="{{route('users.edit', $user->id)}}"><i class="fa fa-pencil"></i> Edit</a>
                <a class="btn btn-primary btn-sm" href="{{route('users.show', $user->id)}}"><i class="fa fa-pencil"></i> View</a>
              {{--  <a class="btn btn-primary btn-sm" href="{{route('users.destroy', $user->id)}}"><i class="fa fa-pencil"></i> Delete</a>--}}
              </td>
            </tr>
            @endforeach
        </tbody>
      </table>
    </div>

{{--    <div class="col-md-2"> <!--Form for creating category -->
      <div class="well">
        {!! Form::open(['route' => 'user.store', 'method' => 'POST']) !!}
                  <br/>          <br/>          <br/>
          <h4 class="text-center text-primary">Create User</h3>

          {{ Form::text('surname', null, ['class' => 'form-control form-spacing', 'placeholder' => 'Surnname'])}}
          {{ Form::textarea('othernames', null, ['class' => 'form-control form-spacing', 'placeholder' => 'Other Names'])}}
          {{ Form::email('email', null, ['class' => 'form-control form-spacing', 'placeholder' => 'Email Address'])}}
          {{ Form::password('password', null, ['class' => 'form-control form-spacing', 'placeholder' => 'Password'])}}
          {{ Form::submit('Add User', ['class' => 'btn btn-primary btn-block form-spacing'])}}

        {!! Form::close() !!}
      </div>
    </div>--}}
  </div>

@endsection
