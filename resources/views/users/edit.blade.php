@extends('partials.uimain')

@section('title', '| Edit Role')


@section('stylesheets')

    {{--Load page specific stylesheets that will only affect this page --}}
    {!! Html::style('css/select2.min.css') !!}

@endsection

@section('content')

    <div class="row space-top">

      <div class="col-6 offset-3">
        {{ Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PUT']) }}
          <br/>          <br/>          <br/>
          <h3 class="text-center text-primary">Edit Permission</h3>
          <h3 class="text-capitalize text-danger">{{ $user->othernames }} {{ $user->surname }}</h3>
          {{ Form::label('roles', 'Select Permission(s)', ['class' => 'form-spacing']) }}
    			{{ Form::select('roles[]', $roles, null, ['class' => 'form-control select2-multi-tags', 'multiple' => 'multiple']) }}
          {{ Form::submit('Save Changes', ['class' => 'btn btn-primary btn-block form-spacing'])}}

        {!! Form::close() !!}
      </div>
      <div class="col-sm-4 offset-4">
						{!! Html::linkRoute('users.index', 'Cancel', array('class' => 'btn btn-danger btn-block')) !!}
			</div>

    </div>

@endsection

@section('javascripts')

	{!! Html::script('js/select2.min.js') !!}

	<script type="text/javascript">
			$('.select2-multi-tags').select2();
	</script>

@endsection
