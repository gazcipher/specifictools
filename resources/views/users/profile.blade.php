@extends('partials.uimain')

@section('title', '| User Profile, Permissions')

@section('content')

  <div class="row space-top">
    <div class="col-6 offset-2">
      <h2 class="text-center">User's Permission(s)</h2>
      <table class="table table-condensed">
        <thead>
          <tr>
            <th>Email Address</th>
            <th>Full Name</th>
            <th>Permission(s)</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
            <tr>
              <td> {{ $user->email }}</td>
              <td> {{ $user->surname }} {{ $user->othernames }}</td>
              <td>
                @foreach($user->roles as $role)
                  <li class="label label-default">{{ $role->name }}</li>
                @endforeach
              </td>
              <td>
                <a class="btn btn-primary btn-sm" href="{{route('users.edit', $user->id)}}"><i class="fa fa-pencil"></i> Edit</a>
              </td>
            </tr>
        </tbody>
      </table>
    </div>


  </div>

@endsection
