@extends('partials.uimain')

@section('title', '| Users')


@section('stylesheets')

    {{--Load page specific stylesheets that will only affect this page --}}


@endsection

@section('content')

  <div class="row space-top">
    <div class="col-4 offset-2">
      <h2>Delete User ?</h2>

      {!! Form::open(['route' => ['users.destory', $user->d], 'method' => 'DELETE']) !!}
        {{Form::submit('Delete', array('class' => 'btn btn-danger btn block'))}}
      {!! Form::close() !!}

    </div>
  </div>

@endsection
