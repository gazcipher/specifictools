<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //show all users
        $users = User::orderBy('created_at', 'desc')->paginate(10);

        return view('users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate and create user
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

        return view('users.profile', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //show editable form for user
        $roles = Role::all();
        $roleList = array();
        foreach ($roles as $role)
        {
          $roleList[$role->id] = $role->name;
        }

        return view('users.edit', ['user' => $user], ['roles' => $roleList]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //validate and update user password
        $this->validate($request, array(
          'roles' => 'required'
        ));

        //sync tag associations
       if(isset($request->roles))
       {
         $user->roles()->sync($request->roles);
       }else{
         $user->roles()->sync(array()); //if not tag select, show empty array
       }

        Session::flash('success', 'Permission was successfuly Updated!');
        //Redirect user to the Post homepage
        return redirect()->route('users.show', $user->id);

    }

    public function delete()
    {
      return view('users.delete');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //delete user
        $user = User::FindOrFail($user->id);
        $user->roles()->detach(); //deassociate permissions

        $user->delete();
        return redirect()->route('users.index')->with('success', 'User Successfuly Deleted');
    }
}
