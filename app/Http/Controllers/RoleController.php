<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Session;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //sho all roles
        $roles = Role::paginate(10);
        return view('roles.index', compact('roles', $roles));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate role
        $this->validate($request, array(
          'name' => ['required', 'min:3', 'max:64', 'string'],
          'role_description' => ['string', 'max:1000']
        ));

        //create role
        Role::create($request->all());

        Session::flash('success', 'New Role Successfully Added');
        return redirect()->route('roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //show a single role
        return view('roles.show', compact('role', $role));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //show edit form
        return view('roles.modify', compact('role', $role));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        //update role , validate first
        $this->validate($request, array(
          'name' => ['required', 'min:3', 'max:64', 'string'],
          'role_description' => ['string', 'max:1000']
        ));

        //create role

        $role->update($request->all());

        Session::flash('success', 'Role Update Success!');
        return redirect()->route('roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //delete role
        //deleting tags and it associate and all Posts linked to it
        try {
          $role = Role::findOrFail($role->id);
          $role->users()->detach(); //remove the role from the users assigned to
        } catch (ModelNotFoundException $e) {
          dd(get_class_methods($e)); // lists all available methods for exception object
          dd($e);
        }
        //delete the actual Tag now
        $role->delete();
        //flash message
        Session::flash('success', 'Role Successfully Deleted');
        return redirect()->route('roles.index');
    }
}
