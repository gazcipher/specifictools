<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException; //used for findOrFail Exception
use Illuminate\Validation\Rule;
use App\News;
use App\Category;
use Illuminate\Http\Request;
use Session;
use Image;
use Purifier;
use Storage; //Storage facade for image deletion or manipulation with directories


class NewsController extends Controller
{

    //protecting actions by auth using the constructor
    public function __construct()
    {
      $this->middleware('auth')->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //show all news
        $news = News::orderBy('created_at', 'desc')->paginate(3);
        return view('news.index', ['news' => $news]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //get news categories
        $categories = Category::all();
        return view('news.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate news
        $this->validate($request, array(
          'title' => 'string|required|min:3',
          'body' =>  'required:min:3',
          'news_slug' => 'required|alpha_dash|min:5|max:255|unique:news', /*no space is allowed and only dash for the url*/
          'category_id' => 'required|integer',
          'image_url' => 'sometimes|image'
        ));

        $news = new News();
        $news->title = $request->title;
        $news->news_slug = $request->news_slug;
        $news->body = Purifier::clean($request->body); //prevent sql injection
        $news->category_id = $request->category_id;

        if($request->hasFile('image_url'))
        {
          $image = $request->file('image_url');
          $filename = time() . '.' .$image->getClientOriginalExtension();
          $location = public_path('news_images/' . $filename);
          Image::make($image)->resize(400,400)->save($location);

          $news->image_url = $filename; //save filename into database
        }

        $news->save();

        Session::flash('success', 'News successfully added!');
        return redirect()->route('pages.single', $news->news_slug);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //get a single article
        return view('news.show', ['news' => $news]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        $news = News::FindOrFail($news->id);

        $categories = Category::all();
        $categoryList = array();
        foreach ($categories as $category)
        {
          $categoryList[$category->id] = $category->categoryName;
        }
        //editing
        return view('news.edit', ['news' => $news], ['categories' => $categoryList]);//->withNews($news);//->withCategories($categoryList);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        //get the news to edit
        $update_news = News::FindOrFail($news->id);

        //validate
        //validate news
        $this->validate($request, array(
          'title' => 'string|required|min:3',
          'body' =>  'required:min:3',
          'category_id' => 'required|integer',
          'image_url' => 'image',
          'news_slug' => "required|alpha_dash|min:5|max:255|unique:news,news_slug,$news->id", //Unique post_slug field
          'news_slug' => "exists:news,$news->id"
        ));

        $update_news->title = $request->title;
        $update_news->news_slug = $request->news_slug;
        $update_news->body = Purifier::clean($request->body); //prevent sql injection
        $update_news->category_id = $request->category_id;

        if($request->hasFile('image_url'))
        {
          $image = $request->file('image_url');
          $filename = time() . '.' .$image->getClientOriginalExtension();
          $location = public_path('news_images/' . $filename);
          Image::make($image)->resize(400,400)->save($location);

          //get old filename
          $oldImage = $update_news->image_url;

          //update the db with new file name
         $update_news->image_url = $filename; //save image url inside the database

         //delete old filename
         Storage::delete($oldImage);
        }

        $update_news->save();

        Session::flash('success', 'News Update successful!');
        return redirect()->route('pages.single', $news->news_slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
      //delete a post and its association to tags
          try {
            $news = News::FindOrFail($news->id);
          } catch (ModelNotFoundException $e) {
            dd(get_class_methods($e)); // lists all available methods for exception object
            dd($e);
          }
          //get the image name and delete it
          Storage::delete($news->image_url);
          $news->delete(); //now delete the post

          return redirect()->route('news.index')->with('success', 'News Successfuly Deleted');
    }

}
