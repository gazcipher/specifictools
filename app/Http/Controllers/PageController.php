<?php

namespace App\Http\Controllers;

use FarhanWazir\GoogleMaps\GMaps;
use Illuminate\Http\Request;
use App\News;

class PageController extends Controller
{
    //

    public function index()
    {
      //get news
      $news = News::orderBy('created_at', 'desc')->paginate(5);
      return view('pages.index', compact('news', $news));
    }

    public function service()
    {
      return view('pages.service');
    }

    public function structure()
    {
      return view('pages.structure');
    }

    public function about()
    {
      return view('pages.aboutus');
    }
    public function leadership()
    {
      return view('pages.leadership');
    }

    public function contact()
    {

      /*  $config['center'] = '51B Adekunle Bisi Street, Hill View Estate Ogudu GRA, Ojota, Lagos.';
        $config['zoom'] = '14';
        $config['map_height'] = '400px';

        $gmap = new GMaps();
        $gmap->initialize($config);

        $map = $gmap->create_map();*/

        //return view('pages.contactus', compact('map', $map));
        return view('pages.contactus');
    }

    public function faq()
    {
      return view('pages.faq');
    }



    //get a single news
    public function getSingleNewsUrl($news_slug)
    {
          //get single
      $news = News::where('news_slug', '=', $news_slug)->first();

      return view('pages.single', ['news' => $news]);
    }

    public function quote()
    {
      return view('pages.quote');
    }

    public function sendquote()
    {
      // code...
    }



}
