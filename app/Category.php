<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //fillable fields
    protected $fillable = [
      'categoryName',
      'description'
    ];


    //one to many relationship, A Category has Many news assigned to it
    public function news()
    {
      return $this->hasMany('App\News');
    }


}
