<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //fillable fields
    protected $fillable = [
      'title', 'news_slug',
      'body', 'category_id',
      'image_url'
    ];


    public function category()
    {
      return $this->belongsTo('App\Category');//one to many relationship, A news belongs to a category Internal, External
    }

}
