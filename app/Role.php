<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //protected fillabes
    protected $fillable = ['name', 'role_description'];







    //===================Many to Many Relationship===========
    public function users()
    {
      return $this->belongsToMany('App\User'); // A user has many roles or belongs to Many roles
    }
}
