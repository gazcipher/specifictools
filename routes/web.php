<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['web']], function(){

    Route::get('/', 'PageController@index')->name('index');
    Route::get('about', 'PageController@about')->name('pages.aboutus');
    Route::get('services', 'PageController@service')->name('pages.service');
    Route::get('structure', 'PageController@structure')->name('pages.structure');
    Route::get('quote', 'PageController@quote')->name('pages.quote');

    Route::get('contact', 'PageController@contact')->name('pages.contactus');
    Route::get('contact/direction', 'PageController@direction');

    Route::get('team', 'PageController@leadership')->name('pages.leadership');
    Route::get('faq', 'PageController@faq')->name('pages.faq');
    Route::get('news-link/{news_slug}', ['as' => 'pages.single', 'uses' => 'PageController@getSingleNewsUrl'])->where('news_slug', '[\w\d\-\_]+');

    Route::resource('categories', 'CategoryController');
    Route::resource('roles', 'RoleController')->except('create');
    Route::resource('news', 'NewsController');
    Route::resource('users', 'UserController')->except('create');


    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');

});
